#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "person.h"
#include <QVector>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_loginBt_clicked();

    void on_pushButton_clicked();

    void on_nextBt_clicked();

    void on_previousBt_clicked();

    void on_NewBt_clicked();

    void on_delBt_clicked();

private:
    Ui::MainWindow *ui;
    QVector<person> data;

    int currentIdx;
    void fillForm();

};

#endif // MAINWINDOW_H

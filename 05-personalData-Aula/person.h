#ifndef PERSON_H
#define PERSON_H

#include <QString>

class person
{
private:
    QString name, email, observ;
public:
    person();
    person(QString name, QString email, QString observ);

    QString getName() const;
    void setName(const QString &value);
    QString getEmail() const;
    void setEmail(const QString &value);
    QString getObserv() const;
    void setObserv(const QString &value);
};

#endif // PERSON_H

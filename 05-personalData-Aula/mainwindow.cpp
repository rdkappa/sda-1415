#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    data.append(person("Margarida", "margarida@ltic.pt","Observ sobre a Margarida...."));
    data.append(person("João", "joao@ltic.pt","Observ sobre o João...."));
    data.append(person("Cláudia", "claudia@ltic.pt","Observ sobre a Cláudia...."));
    currentIdx = 0;
    fillForm();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::fillForm(){
    if (data.length() == 0){
        ui->nameTxt->setText("");
        ui->emailTxt->setText("");
        ui->obsTxt->setPlainText("");

    }else{
        ui->nameTxt->setText(data.at(currentIdx).getName());
        ui->emailTxt->setText(data.at(currentIdx).getEmail());
        ui->obsTxt->setPlainText(data.at(currentIdx).getObserv());
    }
}

void MainWindow::on_loginBt_clicked()
{
    if (ui->loginTxt->text() == "LTIC" && ui->passTxt->text() == "123"){
        ui->stackedWidget->setCurrentIndex(1);
        ui->msgLb->setText("Login OK!");
    }else{
        ui->msgLb->setText("ERROR: wrong login or password");
    }
}

void MainWindow::on_pushButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
    ui->msgLb->setText("Logout...");
}

void MainWindow::on_nextBt_clicked()
{
    if (data.length() == 0) return;
    data[currentIdx].setName(ui->nameTxt->text());
    data[currentIdx].setEmail(ui->emailTxt->text());
    data[currentIdx].setObserv(ui->obsTxt->toPlainText());

    if ( ++currentIdx > data.length() - 1){
        currentIdx = 0;
    }
    fillForm();
}

void MainWindow::on_previousBt_clicked()
{
    if (data.length() == 0) return;

    data[currentIdx].setName(ui->nameTxt->text());
    data[currentIdx].setEmail(ui->emailTxt->text());
    data[currentIdx].setObserv(ui->obsTxt->toPlainText());

    if ( --currentIdx < 0 ){
        currentIdx = data.length() - 1;
    }
    fillForm();
}

void MainWindow::on_NewBt_clicked()
{
    data.append(person("...","...","..."));
    currentIdx = data.length() - 1;
    fillForm();
}

void MainWindow::on_delBt_clicked()
{
    if (data.length() == 0) {
        fillForm();
        return ;
    }
    data.remove(currentIdx);
    if(currentIdx == data.length())
        currentIdx = data.length() - 1;
    fillForm();
}

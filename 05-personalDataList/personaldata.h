#ifndef PERSONALDATA_H
#define PERSONALDATA_H

#include<QString>

class PersonalData
{
private:
    QString name, email, observations;
public:
    PersonalData();
    PersonalData(QString name, QString email, QString observations);

    QString getName() const;
    void setName(const QString &value);
    QString getEmail() const;
    void setEmail(const QString &value);
    QString getObservations() const;
    void setObservations(const QString &value);
};

#endif // PERSONALDATA_H

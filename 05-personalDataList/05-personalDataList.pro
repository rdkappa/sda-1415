#-------------------------------------------------
#
# Project created by QtCreator 2015-04-27T15:57:28
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 05-personalDataList
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    personaldata.cpp

HEADERS  += mainwindow.h \
    personaldata.h

FORMS    += mainwindow.ui

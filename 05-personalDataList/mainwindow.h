#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include "personaldata.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_loginBt_clicked();

    void on_pushButton_clicked();

    void on_previousBt_clicked();

    void fillForm();

    void on_nextBt_clicked();

private:
    Ui::MainWindow *ui;
    QVector<PersonalData> data;
    int active_reg;

};

#endif // MAINWINDOW_H

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->data.append(PersonalData("Margarida", "margarida@ltic.pt", "something about Margarida...."));
    this->data.append(PersonalData("João", "joao@ltic.pt", "something about Joao...."));
    this->data.append(PersonalData("Cláudia", "claudia@ltic.pt", "something about Claudia...."));
    this->active_reg = 0;
    fillForm();
}



MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_loginBt_clicked()
{
    if (ui->loginTxt->text() == "LTIC" && ui->passwordTxt->text() == "123"){
        ui->messageLb->setText("Login OK!");
        ui->stackedWidget->setCurrentIndex(1);
    }else{
        ui->messageLb->setText("ERROR: incorrect login!");
    }
}

void MainWindow::on_pushButton_clicked()
{
    ui->passwordTxt->setText("");
    ui->stackedWidget->setCurrentIndex(0);
    ui->messageLb->setText("");
}

void MainWindow::on_previousBt_clicked()
{
    active_reg--;
    if (active_reg < 0){
        active_reg = data.length() - 1;
    }
    fillForm();

}


void MainWindow::on_nextBt_clicked()
{
    if (++active_reg >= data.length())
        active_reg = 0;
    fillForm();
}


void MainWindow::fillForm(){
    ui->nameTxt->setText(data.at(active_reg).getName());
    ui->emailTxt->setText(data.at(active_reg).getEmail());
    ui->observtxt->setText(data.at(active_reg).getObservations());
}

#include "personaldata.h"

PersonalData::PersonalData()
{
}

PersonalData::PersonalData(QString name, QString email, QString observations)
{
    this->setEmail(email);
    this->setName(name);
    this->setObservations(observations);
}

QString PersonalData::getName() const
{
    return name;
}

void PersonalData::setName(const QString &value)
{
    name = value;
}

QString PersonalData::getEmail() const
{
    return email;
}

void PersonalData::setEmail(const QString &value)
{
    email = value;
}

QString PersonalData::getObservations() const
{
    return observations;
}

void PersonalData::setObservations(const QString &value)
{
    observations = value;
}



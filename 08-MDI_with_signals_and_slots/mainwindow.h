#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>

#include "form.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_personsTable_cellDoubleClicked(int row, int column);
    void receiveDataFromForm(int, QStringList);

    void on_newBt_clicked();

signals:
    void sendData2Form(int, QStringList);

private:
    Ui::MainWindow *ui;
    QSqlDatabase db;
    void fillForm();
    bool filling;
    Form *form;
};

#endif // MAINWINDOW_H
